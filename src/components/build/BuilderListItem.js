import React, { Component } from "react";

class BuilderListItem extends Component {
  returnPartItemRow(partType, part) {
    if (!part) {
      return (
        <tr>
          <th scope="row">{partType}</th>
          <td>Not added yet!</td>
          <td></td>
        </tr>
      );
    }

    const { name, imgUrl, price } = part;

    return (
      <tr>
        <th scope="row">{partType}</th>
        <td>
          <b>{name}</b> {this.returnPartImg(imgUrl)}
        </td>
        <td>&euro;{price}</td>
      </tr>
    );
  }

  returnPartImg(imgUrl) {
    if (imgUrl || imgUrl != null) {
      return (
        <img
          src={imgUrl}
          style={{
            borderWidth: "0.5px",
            borderColor: "lightgrey",
            borderRadius: "0.5px"
          }}
          width="30px"
          height="30px"
          alt="Chosen Part"
        />
      );
    }

    return (
      <img
        src="../../assets/not-found.jpg"
        width="40px"
        height="40px"
        alt="Chosen Part"
      />
    );
  }

  render() {
    const { partType, part } = this.props;

    return this.returnPartItemRow(partType, part);
  }
}

export default BuilderListItem;
