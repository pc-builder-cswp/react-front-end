import React, { Component } from "react";
import "../../App.css";
import BuildsListItem from "./BuildsListItem";
import { ListGroup, Card, CardBody } from "reactstrap";
import { CSSTransition, TransitionGroup } from "react-transition-group";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { getBuilds } from "../../redux/actions/buildActions";
import PropTypes from "prop-types";

class Builds extends Component {
  static propTypes = {
    getBuilds: PropTypes.func.isRequired,
    build: PropTypes.object.isRequired,
    isAuthenticated: PropTypes.bool
  };

  componentDidMount() {
    this.props.getBuilds();
  }

  render() {
    const { builds } = this.props.build;

    return (
      <div>
        <nav className="text-center">
          <div>
            <h1>Builds</h1>
            <p>Check out other builds</p>
          </div>
        </nav>
        <div className="container" style={{ padding: "10px" }}>
          <Card>
            <CardBody>
              {builds.length !== 0 ? (
                <ListGroup>
                  <TransitionGroup className="parts-list">
                    {builds.map(build => (
                      <CSSTransition
                        key={build._id}
                        timeout={500}
                        classNames="fade"
                      >
                        <Link to={`/builds/${build._id}`}>
                          <BuildsListItem buildItem={build} />
                        </Link>
                      </CSSTransition>
                    ))}
                  </TransitionGroup>
                </ListGroup>
              ) : (
                <div className="text-center">
                  <p>
                    Could not get any builds!
                    <br />
                    Possible reasons: Database is empty or internal problems
                  </p>
                </div>
              )}
            </CardBody>
          </Card>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  build: state.build,
  isAuthenticated: state.auth.isAuthenticated
});

export default connect(mapStateToProps, { getBuilds })(Builds);
