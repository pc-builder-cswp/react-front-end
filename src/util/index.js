import jwtDecode from "jwt-decode";
const TOKEN_KEY = "token";

export const isLogin = () => {
  if (localStorage.getItem(TOKEN_KEY)) {
    return true;
  }

  return false;
};

export const getIdFromToken = () => {
  const token = localStorage.getItem(TOKEN_KEY);

  if (token != null) {
    const userId = jwtDecode(token);
    return userId.sub;
  } else {
    return null;
  }
};
