import React, { Component } from "react";
import { Button } from "reactstrap";
import { AvForm, AvField } from "availity-reactstrap-validation";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { login } from "../../redux/actions/authActions";
import { clearErrors } from "../../redux/actions/errorActions";
import { PropTypes } from "prop-types";

class LoginForm extends Component {
  constructor(props) {
    super(props);
    this.handleInvalidSubmit = this.handleInvalidSubmit.bind(this);
    this.handleValidSubmit = this.handleValidSubmit.bind(this);
    this.state = {
      username: false,
      password: false,
      msg: null
    };
  }

  static propTypes = {
    isAuthenticated: PropTypes.bool,
    error: PropTypes.object.isRequired,

    login: PropTypes.func.isRequired,
    clearErrors: PropTypes.func.isRequired
  };

  handleValidSubmit = (event, values) => {
    event.persist();
    this.setState({
      username: values.username,
      password: values.password
    });

    const { username, password } = this.state;

    const user = {
      username,
      password
    };

    // Attempt to login
    this.props.login(user);
  };

  handleInvalidSubmit = (event, errors, values) => {
    this.setState({
      username: values.username,
      password: values.password,
      error: true
    });
    alert("Login credentials are not valid, please try again.");
    console.log("Login failed");
  };

  render() {
    return (
      <AvForm
        onValidSubmit={this.handleValidSubmit}
        onInvalidSubmit={this.handleInvalidSubmit}
      >
        <p>{this.state.msg}</p>
        <AvField
          name="username"
          label="username"
          type="text"
          validate={{
            required: {
              value: true,
              errorMessage: "Please enter your username"
            }
          }}
        />
        <AvField
          name="password"
          label="password"
          type="password"
          validate={{
            required: {
              value: true,
              errorMessage: "Please enter your password"
            }
          }}
        />
        <Button id="submit">Submit</Button>
        <br />
        <Link to="/register">
          <span>I don't have an account yet</span>
        </Link>
      </AvForm>
    );
  }
}

const mapStateToProps = state => ({
  isAuthenticated: state.auth.isAuthenticated,
  error: state.error
});

export default connect(mapStateToProps, { login, clearErrors })(LoginForm);
