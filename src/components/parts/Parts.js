import React, { Component } from "react";
import "../../App.css";
import PartListItem from "./PartListItem";
import { ListGroup, Card, CardBody } from "reactstrap";
import { CSSTransition, TransitionGroup } from "react-transition-group";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { getParts } from "../../redux/actions/partActions";
import PropTypes from "prop-types";

class Parts extends Component {
  static propTypes = {
    getParts: PropTypes.func.isRequired,
    part: PropTypes.object.isRequired,
    isAuthenticated: PropTypes.bool
  };

  componentDidMount() {
    this.props.getParts();
  }

  render() {
    const { parts } = this.props.part;

    return (
      <div>
        <nav className="text-center">
          <h1>Parts</h1>
        </nav>
        <div className="container" style={{ padding: "10px" }}>
          <Card>
            <CardBody>
              {parts.length !== 0 ? (
                <ListGroup>
                  <TransitionGroup className="parts-list">
                    {parts.map(part => (
                      <CSSTransition
                        key={part._id}
                        timeout={500}
                        classNames="fade"
                      >
                        <Link to={`/parts/${part._id}`}>
                          <PartListItem partItem={part} />
                        </Link>
                      </CSSTransition>
                    ))}
                  </TransitionGroup>
                </ListGroup>
              ) : (
                <div className="text-center">
                  <p>
                    Could not get the parts!
                    <br />
                    Possible reasons: Database is empty or internal problems
                  </p>
                </div>
              )}
            </CardBody>
          </Card>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  part: state.part,
  isAuthenticated: state.auth.isAuthenticated
});

export default connect(mapStateToProps, { getParts })(Parts);
