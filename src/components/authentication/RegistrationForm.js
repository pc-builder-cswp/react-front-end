import React, { Component } from "react";
import { Button } from "reactstrap";
import { AvForm, AvField } from "availity-reactstrap-validation";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { register } from "../../redux/actions/authActions";
import { clearErrors } from "../../redux/actions/errorActions";

class RegistrationForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: false,
      password: false,
      emailaddress: false
    };
  }

  handleValidSubmit = (event, values) => {
    event.persist();

    this.setState({
      username: values.username,
      password: values.password,
      emailaddress: values.emailaddress
    });

    const { username, password, emailaddress } = this.state;

    const user = {
      username,
      password,
      emailaddress
    };

    // Attempt to login
    this.props.register(user);
  };

  handleInvalidSubmit = (event, errors, values) => {
    this.setState({
      username: values.username,
      password: values.password,
      emailaddress: values.emailaddress,
      error: true
    });
    alert("Registration credentials are not valid, please try again.");
    console.log("Registration failed");
  };

  render() {
    return (
      <AvForm
        onValidSubmit={this.handleValidSubmit}
        onInvalidSubmit={this.handleInvalidSubmit}
      >
        <AvField
          name="username"
          label="Username"
          type="text"
          placeholder="Required"
          validate={{
            required: {
              value: true,
              errorMessage: "Please enter a username"
            }
          }}
        />
        <AvField
          name="password"
          label="Password"
          type="password"
          placeholder="Required"
          validate={{
            required: {
              value: true,
              errorMessage: "Please enter a password"
            },
            pattern: {
              value: "^(?=.*d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$",
              errorMessage:
                "Minimum eight characters, at least one upper and lowercase and one number"
            }
          }}
        />
        <AvField
          name="emailaddress"
          label="Email Address"
          type="email"
          placeholder="Required"
          validate={{
            required: {
              value: true,
              errorMessage: "Please enter a emailaddress"
            }
          }}
        />
        <Button id="submit">Submit</Button>
        <br />
        <Link to="/login">
          <span>I already have an account</span>
        </Link>
      </AvForm>
    );
  }
}

const mapStateToProps = state => ({
  isAuthenticated: state.auth.isAuthenticated,
  error: state.error
});

export default connect(mapStateToProps, { register, clearErrors })(
  RegistrationForm
);
