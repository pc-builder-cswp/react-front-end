import React, { Component } from "react";
import { ListGroup, ListGroupItem, Card, CardBody, Button } from "reactstrap";
import { AvForm, AvField } from "availity-reactstrap-validation";
import { CSSTransition, TransitionGroup } from "react-transition-group";
import {
  getPartComments,
  getBuildComments,
  createComment,
  deleteComment,
  updateComment
} from "../../redux/actions/commentActions";
import { BUILD } from "../../helpers/objectTypesConstants";
import PropTypes from "prop-types";
import { connect } from "react-redux";

import CommentListItem from "./CommentListItem";

class CommentList extends Component {
  constructor(props) {
    super(props);
    this.state = { content: null };
    this.handleValidSubmit = this.handleValidSubmit.bind(this);
    this.handleInvalidSubmit = this.handleInvalidSubmit.bind(this);
  }

  componentDidMount() {
    console.log(this.props.componentType + " | " + this.props.componentId);

    if (this.props.componentType === BUILD) {
      this.props.getBuildComments(this.props.componentId);
    } else {
      this.props.getPartComments(this.props.componentId);
    }
  }

  handleValidSubmit = (event, values) => {
    event.persist();
    this.setState({
      content: values.content
    });

    const { content } = this.state;

    let comment = {};

    if (this.props.componentType === BUILD) {
      comment = {
        buildId: this.props.componentId,
        content: content
      };
    } else {
      comment = {
        partId: this.props.componentId,
        content: content
      };
    }

    // Attempt to create comment
    this.props.createComment(comment);

    this.setState({ content: null });
  };

  handleInvalidSubmit = (event, errors, values) => {
    this.setState({
      content: values.content
    });
    alert("Comment is invalid, please try again.");
    console.log("Comment create failed");
  };

  returnUserOptions() {
    return <div></div>;
  }

  render() {
    const { comments } = this.props.comment;

    return (
      <div style={{ marginTop: "5%" }}>
        <div>
          <h4>Comments</h4>
        </div>
        <div>
          <Card style={{ marginTop: "20px", marginBottom: "20px" }}>
            <CardBody>
              <AvForm
                onValidSubmit={this.handleValidSubmit}
                onInvalidSubmit={this.handleInvalidSubmit}
              >
                <AvField
                  name="content"
                  label="Leave a comment..."
                  type="textarea"
                  placeholder="Enter here your comment"
                  validate={{
                    required: {
                      value: true,
                      errorMessage: "Comment is empty"
                    }
                  }}
                />
                <Button id="submit">Submit</Button>
              </AvForm>
            </CardBody>
            <CardBody>
              {comments.length !== 0 ? (
                <ListGroup>
                  <TransitionGroup className="parts-list">
                    {comments.map(comment => (
                      <CSSTransition
                        key={comment._id}
                        timeout={500}
                        classNames="fade"
                      >
                        <ListGroupItem
                          style={{
                            padding: "10px",
                            marginTop: "20px",
                            borderRadius: "5px"
                          }}
                        >
                          <CommentListItem commentItem={comment} />
                        </ListGroupItem>
                      </CSSTransition>
                    ))}
                  </TransitionGroup>
                </ListGroup>
              ) : (
                <div className="text-center">
                  <p>
                    Could not get the comments!
                    <br />
                    Possible reasons: Database is empty, internal problems or
                    there are simply no comments.
                  </p>
                </div>
              )}
            </CardBody>
          </Card>
        </div>
      </div>
    );
  }

  static propTypes = {
    getPartComments: PropTypes.func.isRequired,
    getBuildComments: PropTypes.func.isRequired,
    createComment: PropTypes.func.isRequired,
    deleteComment: PropTypes.func.isRequired,
    updateComment: PropTypes.func.isRequired,
    comment: PropTypes.object.isRequired,
    isAuthenticated: PropTypes.bool
  };
}

const mapStateToProps = state => ({
  comment: state.comment,
  isAuthenticated: state.auth.isAuthenticated
});

export default connect(mapStateToProps, {
  getBuildComments,
  getPartComments,
  createComment,
  deleteComment,
  updateComment
})(CommentList);
