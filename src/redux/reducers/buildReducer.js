import {
  GET_BUILD,
  BUILD_LOADING,
  CREATE_BUILD,
  DELETE_BUILD,
  GET_BUILDS,
  BUILDS_LOADING
} from "../actions/types";

const initialState = {
  builds: [],
  build: null,
  loading: false
};

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_BUILD:
      return {
        ...state,
        build: action.payload,
        loading: false
      };
    case BUILD_LOADING:
      return {
        ...state,
        loading: true
      };
    case CREATE_BUILD:
      return {
        ...state,
        builds: [action.payload, ...state.builds]
      };
    case DELETE_BUILD:
      return {
        ...state,
        builds: state.builds.filter(build => build._id !== action.payload)
      };
    case GET_BUILDS:
      return {
        ...state,
        builds: action.payload,
        loading: false
      };
    case BUILDS_LOADING:
      return {
        ...state,
        loading: true
      };
    default:
      return state;
  }
}
