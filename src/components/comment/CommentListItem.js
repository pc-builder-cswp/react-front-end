import React, { Component } from "react";

class CommentListItem extends Component {
  render() {
    const { commentItem } = this.props;

    return (
      <div>
        <div>
          <small>{commentItem.postDate}</small>
        </div>
        <div>
          <small>
            <b>{commentItem.user.username}:</b>
          </small>
        </div>
        <div>
          <p>{commentItem.content}</p>
        </div>
      </div>
    );
  }
}

export default CommentListItem;
