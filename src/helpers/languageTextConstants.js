export const MORNING = "Good Morning";
export const AFTERNOON = "Good Afternoon";
export const EVENING = "Good Evening";
export const NIGHT = "Good Night";
export const GREETING = "Greetings";
