import React, { Component } from "react";
import { Container, Card, CardBody } from "reactstrap";
import { Link } from "react-router-dom";
import PasswordResetImage from "../../assets/password-reset.png";
import "../../App.css";

class Account extends Component {
  render() {
    return (
      <div>
        <nav>
          <div>
            <h1>Account</h1>
            <p>Manage your account</p>
          </div>
        </nav>
        <Container style={{ paddingTop: "20px", paddingBottom: "20px" }}>
          <Link to="/account/passwordReset">
            <Card>
              <CardBody className="text-center">
                <div>
                  <img
                    src={PasswordResetImage}
                    alt="Password Reset"
                    width="80px"
                    height="80px"
                  />
                </div>
                <div>
                  <h5>Reset Password</h5>
                </div>
              </CardBody>
            </Card>
          </Link>
        </Container>
      </div>
    );
  }
}

export default Account;
