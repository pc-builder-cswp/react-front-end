import React, { Component } from "react";
import { Container, Card, Row, Col, Jumbotron } from "reactstrap";
import { getPart } from "../../redux/actions/partActions";
import CommentList from "../comment/CommentList";
import { PART } from "../../helpers/objectTypesConstants";
import { connect } from "react-redux";

class PartDetail extends Component {
  getParamsId() {
    const {
      match: { params }
    } = this.props;

    return params.id;
  }

  componentDidMount() {
    this.props.getPart(this.getParamsId());
  }

  returnImage(imgUrl) {
    if (imgUrl) {
      return (
        <img
          src={imgUrl}
          style={{
            borderRadius: "5px",
            borderWidth: "1px",
            borderColor: "lightgrey"
          }}
          alt="part"
          width="200px"
          height="200px"
        />
      );
    } else {
      return <img src="../../assets/not-found.jpg" alt="part" />;
    }
  }

  render() {
    const {
      _id,
      name,
      partType,
      manufacturer,
      price,
      imgUrl,
      description,
      powerUsage,
      ram,
      size
    } = this.props.part.part;

    console.log(JSON.stringify(this.props.part.part._id));

    return (
      <Container>
        <div>
          <Card>
            <Jumbotron>
              <Row>
                <Col>
                  <h3>{name}</h3>
                  <p>Type: {partType}</p>
                  <Row />
                  <p>Manufactured by: {manufacturer}</p>
                  <Row />
                  <p>Price: &euro;{price}</p>
                  <Row />
                  <p>Power usage: {powerUsage} Watt</p>
                  <Row />
                  <p>Ram: {ram}GB</p>
                  <Row />
                  <p>Size: {size}</p>
                </Col>
                <Col>{this.returnImage(imgUrl)}</Col>
              </Row>
            </Jumbotron>
            <Container>
              <h5>
                <b>Description</b>
              </h5>
              <Row />
              <p>{description}</p>
            </Container>
          </Card>
          <div>
            <CommentList
              componentId={this.getParamsId() || _id}
              componentType={PART}
            />
          </div>
        </div>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  part: state.part,
  isAuthenticated: state.auth.isAuthenticated
});

export default connect(mapStateToProps, { getPart })(PartDetail);
