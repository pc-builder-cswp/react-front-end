import React, { Component, Fragment } from "react";
import "../App.css";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import Logout from "./authentication/Logout";
import PropTypes from "prop-types";

export class NavigationBar extends Component {
  static propTypes = {
    auth: PropTypes.object.isRequired
  };

  render() {
    const { isAuthenticated } = this.props.auth;

    const logoutLink = <Logout />;

    const aboutLink = (
      <Link style={{ color: "white" }} to="/about">
        <li>About</li>
      </Link>
    );

    return (
      <Fragment>
        <nav>
          <Link to="/">
            <img
              className="navImage"
              alt="PC-Builder"
              src="../assets/pc-builder-logo.PNG"
            />
          </Link>
          <h3>PC Builder</h3>
          <ul className="nav-links">
            {isAuthenticated ? aboutLink : null}
            {isAuthenticated ? logoutLink : null}
          </ul>
        </nav>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(mapStateToProps, null)(NavigationBar);
