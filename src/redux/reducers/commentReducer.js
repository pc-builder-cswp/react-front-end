import {
  CREATE_COMMENT,
  COMMENTS_LOADING,
  GET_COMMENTS,
  UPDATING_COMMENT,
  COMMENT_UPDATED,
  DELETE_COMMENT
} from "../actions/types";

const initialState = {
  comments: [],
  comment: {},
  loading: false
};

export default function(state = initialState, action) {
  switch (action.type) {
    case CREATE_COMMENT:
      return {
        ...state,
        comments: [action.payload, ...state.comments]
      };
    case COMMENTS_LOADING:
      return {
        ...state,
        loading: true
      };
    case GET_COMMENTS:
      return {
        ...state,
        comments: action.payload
      };
    case UPDATING_COMMENT:
      return {
        ...state,
        loading: true
      };
    case COMMENT_UPDATED:
      return {
        comment: action.payload
      };
    case DELETE_COMMENT:
      return {
        comments: state.comments.filter(
          comment => comment._id !== action.payload
        )
      };

    default:
      return state;
  }
}
