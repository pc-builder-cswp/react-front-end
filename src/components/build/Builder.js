import React, { Component } from "react";
import BuilderListItem from "./BuilderListItem";
import {
  Row,
  Container,
  Card,
  CardBody,
  ListGroup,
  ListGroupItem,
  Button,
  Table
} from "reactstrap";
import { AvForm, AvField } from "availity-reactstrap-validation";
import { createBuild } from "../../redux/actions/buildActions";
import { getParts } from "../../redux/actions/partActions";
import { connect } from "react-redux";
import PropTypes from "prop-types";

import {
  CPU,
  CPU_COOLER,
  MOTHERBOARD,
  RAM,
  PSU,
  STORAGE,
  PC_CASE,
  GPU
} from "../../helpers/partTypeConstants";

class Builder extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: null,
      description: null,
      cpu: null,
      cpuCooler: null,
      motherboard: null,
      ram: null,
      psu: null,
      storage: null,
      pcCase: null,
      gpu: null,

      partsList: [],
      filteredPartsList: []
    };
  }

  static propTypes = {
    getParts: PropTypes.func.isRequired,
    part: PropTypes.object.isRequired,
    isAuthenticated: PropTypes.bool
  };

  componentDidMount() {
    Promise.all([this.props.getParts()]).then(() => {
      this.setState({
        partsList: this.props.part.parts,
        filteredPartsList: this.props.part.parts
      });
      console.log(JSON.stringify(this.state.filteredPartsList));
    });
  }

  buildIsValid() {
    let {
      title,
      description,
      cpu,
      cpuCooler,
      motherboard,
      ram,
      psu,
      storage,
      pcCase,
      gpu
    } = this.state;

    if (
      title != null &&
      description != null &&
      cpu != null &&
      cpuCooler != null &&
      motherboard != null &&
      ram != null &&
      psu != null &&
      storage != null &&
      pcCase != null &&
      gpu != null
    ) {
      return true;
    }

    return false;
  }

  getCompleteBuild() {
    let {
      title,
      description,
      cpu,
      cpuCooler,
      motherboard,
      ram,
      psu,
      storage,
      pcCase,
      gpu
    } = this.state;

    const build = {
      title: title,
      description: description,
      cpu: cpu._id,
      cpuCooler: cpuCooler._id,
      motherboard: motherboard._id,
      memory: ram._id,
      psu: psu._id,
      storage: storage._id,
      pcCase: pcCase._id,
      gpu: gpu._id
    };

    return build;
  }

  handleResetBuild() {
    this.setState({
      title: null,
      description: null,
      cpu: null,
      cpuCooler: null,
      motherboard: null,
      ram: null,
      psu: null,
      storage: null,
      pcCase: null,
      gpu: null
    });

    console.log("Build state reset!");
  }

  addPartToBuildState(part) {
    console.log("PART" + JSON.stringify(part));
    console.log("TYPE: " + part.partType);

    switch (part.partType) {
      case CPU:
        this.setState({ cpu: part });
        break;
      case CPU_COOLER:
        this.setState({ cpuCooler: part });
        break;
      case MOTHERBOARD:
        this.setState({ motherboard: part });
        break;
      case RAM:
        this.setState({ ram: part });
        break;
      case PSU:
        this.setState({ psu: part });
        break;
      case STORAGE:
        this.setState({ storage: part });
        break;
      case PC_CASE:
        this.setState({ pcCase: part });
        break;
      case GPU:
        this.setState({ gpu: part });
        break;

      default:
        break;
    }

    window.scrollTo(0, 0);
  }

  handleValidSubmit = (event, values) => {
    event.persist();

    this.setState({
      title: values.title,
      description: values.description
    });
    if (this.buildIsValid()) {
      console.log(this.buildIsValid());
      this.props.createBuild(this.getCompleteBuild());
    }
  };

  handleInvalidSubmit = (event, values, error) => {
    this.setState({
      title: values.title,
      description: values.description,

      error: true
    });

    if (!this.buildIsValid()) {
      alert(
        "Build not completed yet. Please complete the build before submitting."
      );
    } else {
      alert(
        "Title and/or description are/is not filled in yet. Please fill in the title and/or the description."
      );
    }
  };

  filterPartsList(partType) {
    var filteredList = this.props.part.parts.filter(function(part) {
      return part.partType === partType;
    });

    this.setState({ filteredPartsList: filteredList });
  }

  returnImage = imgUrl => {
    if (imgUrl == null) {
      return (
        <img
          src="../../assets/not-found.jpg"
          alt="part"
          width="63px"
          height="60px"
        />
      );
    } else {
      return (
        <Container>
          <img
            style={{
              justifyContent: "center",
              alignItems: "center",
              flex: 1
            }}
            src={imgUrl}
            alt="part"
            width="63px"
            height="60px"
          />
        </Container>
      );
    }
  };

  render() {
    const {
      cpu,
      cpuCooler,
      motherboard,
      ram,
      psu,
      storage,
      pcCase,
      gpu
    } = this.state;

    let { parts } = this.props.part;

    return (
      <div>
        <nav>
          <div className="text-center">
            <h1>System Builder</h1>
            <p>Build a new system and share it with the community</p>
          </div>
        </nav>
        <Container>
          <Container style={{ marginBottom: "10px" }}>
            <Table
              size="xs-col-12"
              style={{ marginTop: "10px", marginBottom: "10px" }}
            >
              <thead>
                <tr>
                  <th>Component</th>
                  <th>Selection</th>
                  <th>Price</th>
                </tr>
              </thead>
              <tbody>
                <BuilderListItem partType="CPU" part={cpu} />
                <BuilderListItem partType="CPU Cooler" part={cpuCooler} />
                <BuilderListItem partType="Motherboard" part={motherboard} />
                <BuilderListItem partType="Memory" part={ram} />
                <BuilderListItem partType="Storage" part={storage} />
                <BuilderListItem partType="Video Card" part={gpu} />
                <BuilderListItem partType="Case" part={pcCase} />
                <BuilderListItem partType="Power Supply" part={psu} />
              </tbody>
            </Table>
            <Button onClick={() => this.handleResetBuild()}>Reset Build</Button>
            <Row />
            <div style={{ marginTop: "30px", marginBottom: "30px" }}>
              <AvForm
                onValidSubmit={this.handleValidSubmit}
                onInvalidSubmit={this.handleInvalidSubmit}
              >
                <AvField
                  name="title"
                  label="Title of the build"
                  type="text"
                  placeholder="Required"
                  validate={{
                    required: {
                      value: true,
                      errorMessage: "Please enter a title"
                    }
                  }}
                />
                <AvField
                  name="description"
                  label="Description of the build"
                  type="textarea"
                  placeholder="Required"
                  validate={{
                    required: {
                      value: true,
                      errorMessage:
                        "Please fill in the description of your build"
                    }
                  }}
                />
                <Button id="submit">Submit</Button>
              </AvForm>
            </div>
            <Row />
            <div style={{ paddingTop: "20px", paddingBottom: "20px" }}>
              <h5>
                <b>Choose the parts for the build</b>
              </h5>
              {/* <div>
              <p>Filter specific part</p>
              <div className="builder-partType-btn-group">
                <Button onClick={() => this.filterPartsList(CPU)}>CPU</Button>
                <Button onClick={() => this.filterPartsList(CPU_COOLER)}>
                  CPU Cooler
                </Button>
                <Button onClick={() => this.filterPartsList(MOTHERBOARD)}>
                  Motherboard
                </Button>
                <Button onClick={() => this.filterPartsList(RAM)}>
                  Memory
                </Button>
                <Button onClick={() => this.filterPartsList(PSU)}>
                  Power Supply
                </Button>
                <Button onClick={() => this.filterPartsList(STORAGE)}>
                  Storage
                </Button>
                <Button onClick={() => this.filterPartsList(PC_CASE)}>
                  PC Case
                </Button>
                <Button onClick={() => this.filterPartsList(GPU)}>
                  Video Card
                </Button>
              </div>
            </div> */}
              <Row />
              <div style={{ paddingTop: "10px" }}>
                <Card>
                  <CardBody>
                    <ListGroup>
                      {parts.map(part => (
                        <ListGroupItem
                          key={part._id}
                          style={{ flexDirection: "row", flex: 1 }}
                        >
                          <Row style={{ padding: "20px" }}>
                            <div className="col-6">
                              <p>
                                <b>{part.name}</b>
                              </p>
                              <p>type: {part.partType}</p>
                              <small>&euro;{part.price}</small>
                            </div>
                            <div className="col-4 text-center">
                              {this.returnImage(part.imgUrl)}
                            </div>
                            <div className="col-2 text-center">
                              <Button
                                style={{
                                  borderRadius: "50%",
                                  backgroundColor: "#009BB8"
                                }}
                                onClick={() => this.addPartToBuildState(part)}
                              >
                                <b>+</b>
                              </Button>
                            </div>
                          </Row>
                        </ListGroupItem>
                      ))}
                    </ListGroup>
                  </CardBody>
                </Card>
              </div>
            </div>
          </Container>
        </Container>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  isAuthenticated: state.auth.isAuthenticated,
  build: state.build,
  part: state.part
});

export default connect(mapStateToProps, { getParts, createBuild })(Builder);
