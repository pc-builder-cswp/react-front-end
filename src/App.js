import React, { Component } from "react";
import "./App.css";
import jwtDecode from "jwt-decode";

// Components
import NavigationBar from "./components/Nav";
import Footer from "./components/Footer";
import About from "./components/about/About";
import Parts from "./components/parts/Parts";
import PartDetail from "./components/parts/PartDetail";
import Home from "./components/home/Home";
import Account from "./components/account/Account";
import PasswordReset from "./components/account/PasswordReset";
import Login from "./components/authentication/Login";
import Registration from "./components/authentication/Registration";
import Builds from "./components/build/Builds";
import Builder from "./components/build/Builder";
import BuildDetail from "./components/build/BuildDetail";

// Redux
import { Provider } from "react-redux";
import store from "./redux/store";
import { logout, loadUser } from "./redux/actions/authActions";
import { LOGIN_SUCCESS, AUTH_ERROR } from "./redux/actions/types";

import { Router, Switch, Route, Redirect } from "react-router-dom";
import Axios from "axios";

// Helpers
import { history } from "./helpers/history";
import PrivateRoute from "./helpers/privateRoute";
import PublicRoute from "./helpers/publicRoute";

Axios.defaults.baseURL = "https://pc-builder-api.herokuapp.com/";

const token = localStorage.getItem("token");
console.log("TOKEN: " + token);
if (token != null) {
  const decodedToken = jwtDecode(token);
  console.log("DECODED_TOKEN: " + JSON.stringify(decodedToken));
  if (decodedToken.exp * 1000 < Date.now()) {
    localStorage.clear();
    store.dispatch(logout);
  } else {
    store.dispatch({ type: LOGIN_SUCCESS });
    store.dispatch(loadUser);
  }
} else {
  store.dispatch({ type: AUTH_ERROR });
  store.dispatch(logout);
}

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Router history={history}>
          <div className="App">
            <NavigationBar />
            <Switch>
              <PrivateRoute path="/" exact component={Home} />
              <PrivateRoute path="/about" exact component={About} />
              <PrivateRoute path="/builds" exact component={Builds} />
              <PrivateRoute path="/builds/:id" exact component={BuildDetail} />
              <PrivateRoute path="/builder" exact component={Builder} />
              <PrivateRoute path="/parts" exact component={Parts} />
              <PrivateRoute path="/parts/:id" exact component={PartDetail} />
              <PrivateRoute path="/account" exact component={Account} />
              <PrivateRoute
                path="/account/passwordReset"
                exact
                component={PasswordReset}
              />
              <PublicRoute
                restricted={true}
                path="/login"
                exact
                component={Login}
              />
              <PublicRoute
                restricted={true}
                path="/register"
                exact
                component={Registration}
              />
              <Route render={() => <Redirect to="/" />} />
            </Switch>
            <Footer />
          </div>
        </Router>
      </Provider>
    );
  }
}

export default App;
