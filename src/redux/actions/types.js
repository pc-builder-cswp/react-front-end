export const GET_PARTS = "GET_PARTS";
export const PARTS_LOADING = "PARTS_LOADING";

export const GET_PART = "GET_PART";
export const PART_LOADING = "PART_LOADING";
export const FAVORITING_PART = "FAVORITING_PART";

export const GET_BUILDS = "GET_BUILDS";
export const BUILDS_LOADING = "BUILDS_LOADING";

export const BUILD_LOADING = "BUILD_LOADING";
export const GET_BUILD = "GET_BUILD";
export const CREATE_BUILD = "CREATE_BUILD";
export const UPDATING_BUILD = "UPDATING_BUILD";
export const BUILD_UPDATED = "BUILD_UPDATED";
export const DELETE_BUILD = "DELETE_BUILD";

export const UPVOTING_BUILD = "UPVOTING_BUILD";

export const GET_COMMENT = "GET_COMMENT";
export const COMMENT_LOADING = "COMMENT_LOADING";
export const CREATE_COMMENT = "CREATE_COMMENT";
export const UPDATING_COMMENT = "UPDATING_COMMENT";
export const COMMENT_UPDATED = "COMMENT_UPDATED";
export const DELETE_COMMENT = "DELETE_COMMENT";

export const GET_COMMENTS = "GET_COMMENTS";
export const COMMENTS_LOADING = "COMMENTS_LOADING";

export const USER_LOADING = "USER_LOADING";
export const USER_LOADED = "USER_LOADED";

export const UPDATING_PASSWORD = "UPDATING_PASSWORD";
export const PASSWORD_UPDATED = "PASSWORD_UPDATED";

export const AUTH_ERROR = "AUTH_ERROR";
export const LOGIN_SUCCESS = "LOGIN_SUCCESS";
export const LOGIN_FAIL = "LOGIN_FAIL";
export const LOGOUT_SUCCESS = "LOGOUT_SUCCESS";
export const REGISTER_SUCCESS = "REGISTER_SUCCESS";
export const REGISTER_FAIL = "REGISTER_FAIL";

export const CLEAR_ERRORS = "CLEAR_ERRORS";
export const GET_ERRORS = "GET_ERRORS";
