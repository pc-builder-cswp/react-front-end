import React, { Component } from "react";
import { Nav } from "reactstrap";

class Footer extends Component {
  getCurrentYear() {
    var currentDate = new Date();
    return currentDate.getFullYear();
  }

  render() {
    return (
      <Nav
        style={{
          textAlign: "center",
          background: "#727272",
          color: "lightgrey",
          flexDirection: "column",
          paddingTop: "25px",
          paddingBottom: "25px",
          position: "relative",
          bottom: 0,
          width: "100%"
        }}
      >
        <small>
          © {this.getCurrentYear()} Copyright:
          <span>
            {" "}
            <a
              style={{ color: "white" }}
              href="https://pc-builder-cswp-2-2.herokuapp.com"
            >
              pc-builder-cswp-2-2.herokuapp.com
            </a>{" "}
          </span>
        </small>
        <small>
          <span>
            Part images powered by{" "}
            <a style={{ color: "white" }} href="https://tweakers.net/">
              Tweakers
            </a>{" "}
            | Not for commercial use
          </span>
        </small>
      </Nav>
    );
  }
}

export default Footer;
