import React, { Component } from "react";
import { Row, Col, Jumbotron, Card, CardBody, Button } from "reactstrap";
import { AvForm, AvField } from "availity-reactstrap-validation";
import { resetPassword } from "../../redux/actions/authActions";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import "../../App.css";

class PasswordReset extends Component {
  constructor() {
    super();
    this.handleValidSubmit = this.handleValidSubmit.bind(this);
    this.handleInvalidSubmit = this.handleInvalidSubmit.bind(this);
    this.state = {
      password: null,
      newPassword: null
    };
  }

  static propTypes = {
    isAuthenticated: PropTypes.bool,
    error: PropTypes.object.isRequired,

    resetPassword: PropTypes.func.isRequired,
    clearErrors: PropTypes.func.isRequired
  };

  handleValidSubmit = (event, values) => {
    event.persist();

    this.setState({
      password: values.password,
      newPassword: values.newPassword
    });

    const { password, newPassword } = this.state;

    const passObj = {
      password,
      newPassword
    };

    // Attempt to login
    this.props.resetPassword(passObj);
  };

  handleInvalidSubmit = (event, errors, values) => {
    this.setState({
      password: values.password,
      newPassword: values.newPassword,
      error: true
    });
    alert("New password is not valid, Please try again.");
    console.log("Registration failed");
  };

  render() {
    return (
      <Row>
        <Col />
        <Col lg="8">
          <Jumbotron>
            <h3>Reset your password</h3>
            <hr />
            <Card>
              <CardBody>
                <AvForm
                  onValidSubmit={this.handleValidSubmit}
                  onInvalidSubmit={this.handleInvalidSubmit}
                >
                  <AvField
                    name="password"
                    label="Enter your current password"
                    type="password"
                    placeholder="Current password"
                    validate={{
                      required: {
                        value: true,
                        errorMessage: "Please enter your current password"
                      }
                    }}
                  />
                  <AvField
                    name="newPassword"
                    label="Enter your new password"
                    type="password"
                    placeholder="New password"
                    validate={{
                      required: {
                        value: true,
                        errorMessage: "Please enter your new password"
                      },
                      pattern: {
                        value: "^(?=.*d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$",
                        errorMessage:
                          "Minimum eight characters, at least one upper and lowercase and one number"
                      }
                    }}
                  />
                  <Button id="submit">Submit</Button>
                </AvForm>
              </CardBody>
            </Card>
          </Jumbotron>
        </Col>
        <Col />
      </Row>
    );
  }
}

const mapStateToProps = state => ({
  isAuthenticated: state.auth.isAuthenticated,
  error: state.error
});

export default connect(mapStateToProps, { resetPassword })(PasswordReset);
