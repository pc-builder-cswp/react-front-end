import Axios from "axios";
import {
  GET_COMMENTS,
  COMMENTS_LOADING,
  CREATE_COMMENT,
  COMMENT_LOADING,
  UPDATING_COMMENT,
  COMMENT_UPDATED,
  DELETE_COMMENT
} from "./types";
import { tokenConfig } from "./authActions";
import { returnErrors } from "./errorActions";

export const getPartComments = id => dispatch => {
  dispatch(setCommentsLoading());
  Axios({
    method: "get",
    url: `/apiv1/parts/${id}/comments`,
    headers: tokenConfig().headers
  })
    .then(res => {
      console.log(JSON.stringify(res.data));
      dispatch({
        type: GET_COMMENTS,
        payload: res.data
      });
    })
    .catch(err =>
      dispatch(returnErrors(err.response.data, err.response.status))
    );
};

export const getBuildComments = id => dispatch => {
  dispatch(setCommentsLoading());
  Axios({
    method: "get",
    url: `/apiv1/builds/${id}/comments`,
    headers: tokenConfig().headers
  })
    .then(res =>
      dispatch({
        type: GET_COMMENTS,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch(returnErrors(err.response.data, err.response.status))
    );
};

export const createComment = comment => dispatch => {
  console.log(JSON.stringify(comment));
  Axios({
    method: "post",
    url: `/apiv1/comments`,
    data: comment,
    headers: tokenConfig().headers
  })
    .then(res => {
      dispatch({
        type: CREATE_COMMENT,
        payload: res.data.comment
      });
    })
    .catch(err => {
      console.log(JSON.stringify(err.response));
      dispatch(returnErrors(err.response.data, err.response.status));
    });
};

export const updateComment = (id, comment) => dispatch => {
  dispatch(setUpdatingComment());
  Axios({
    method: "put",
    url: `/apiv1/comments/${id}`,
    data: comment,
    headers: tokenConfig().headers
  })
    .then(res => {
      dispatch({
        type: COMMENT_UPDATED,
        payload: res.data.comment
      });
    })
    .catch(err =>
      dispatch(returnErrors(err.response.data, err.response.status))
    );
};

export const deleteComment = id => dispatch => {
  Axios({
    method: "delete",
    url: `/apiv1/comments/${id}`,
    headers: tokenConfig().headers
  })
    .then(res => {
      dispatch({
        type: DELETE_COMMENT,
        payload: res.data.comment
      });
    })
    .catch(err =>
      dispatch(returnErrors(err.response.data, err.response.status))
    );
};

export const setCommentsLoading = () => {
  return {
    type: COMMENTS_LOADING
  };
};

export const setCommentLoading = () => {
  return {
    type: COMMENT_LOADING
  };
};

export const setUpdatingComment = () => {
  return {
    type: UPDATING_COMMENT
  };
};
