import { combineReducers } from "redux";
import buildReducer from "./buildReducer";
import partReducer from "./partReducer";
import commentReducer from "./commentReducer";
import authReducer from "./authReducer";
import errorReducer from "./errorReducer";

export default combineReducers({
  build: buildReducer,
  part: partReducer,
  comment: commentReducer,
  auth: authReducer,
  error: errorReducer
});
