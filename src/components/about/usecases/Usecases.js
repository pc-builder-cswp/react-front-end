import React, { Component } from "react";
import UsecaseItem from "./UsecaseItem";
import PropTypes from "prop-types";

class Usecases extends Component {
  render() {
    return this.props.usecases.map(usecase => (
      <UsecaseItem usecase={usecase} />
    ));
  }
}

// PropTypes
Usecases.propTypes = {
  usecases: PropTypes.array.isRequired
};

export default Usecases;
