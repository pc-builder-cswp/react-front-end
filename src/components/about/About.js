import React, { Component } from "react";
import { Card } from "reactstrap";
import "../../App.css";

import Usecases from "./usecases/Usecases";

import studentImage from "../../assets/student.jpg";
import { USECASES } from "../../usecase-data";
import { ENTITIES } from "../../entities-data";

class About extends Component {
  constructor() {
    super();
    this.state = {
      ent1: ENTITIES[0],
      ent2: ENTITIES[1],
      ent3: ENTITIES[2],
      ent4: ENTITIES[3]
    };
  }

  render() {
    return (
      <Card>
        <div className="container" style={{ paddingTop: "20px" }}>
          <div className="row">
            <div className="col-10">
              <div>
                <h1>Over deze applicatie</h1>
                <p>
                  Deze applicatie is gemaakt in het kader van het vak
                  'Clientside web frameworks' van Avans Hogeschool Breda.
                </p>
              </div>
              <div>
                <h2>PC-Builder App</h2>
                <p>
                  In dit project is het mogelijk om een PC samen te stellen en
                  is het ook mogelijk om jouw Builds met anderen te delen.
                  Doormiddel van onderdelen kan een build samengesteld worden.
                  Ook is het mogelijk om comments achter te laten bij zowel
                  builds als onderdelen.
                </p>
              </div>
            </div>
            <div className="card col-2">
              <div className="studentCard">
                <img
                  src={studentImage}
                  alt="Bryan Kho"
                  className="studentImage"
                />
                <p className="text-center">Bryan Kho</p>
                <small className="text-center">Studentnummer: 2136024</small>
              </div>
            </div>
          </div>
          <div style={{ paddingTop: "20px" }}>
            <h1>Entities</h1>
            <div className="row entities">
              <div className="col-2 card">
                {this.state.ent1.map(entity => (
                  <div>
                    <p>{entity.toString()}</p>
                  </div>
                ))}
              </div>
              <div className="col-2 card">
                {this.state.ent2.map(entity => (
                  <div>
                    <p>{entity.toString()}</p>
                  </div>
                ))}
              </div>
              <div className="col-2 card">
                {this.state.ent3.map(entity => (
                  <div>
                    <p>{entity.toString()}</p>
                  </div>
                ))}
              </div>
              <div className="col-2 card">
                {this.state.ent4.map(entity => (
                  <div>
                    <p>{entity.toString()}</p>
                  </div>
                ))}
              </div>
            </div>
          </div>
          <div style={{ paddingTop: "40px" }}>
            <h1>Use Cases</h1>
            <div>
              <Usecases usecases={USECASES} />
            </div>
          </div>
        </div>
      </Card>
    );
  }
}

export default About;
