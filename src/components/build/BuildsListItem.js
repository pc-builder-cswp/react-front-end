import React, { Component } from "react";
import { ListGroupItem, Row, Button, Col } from "reactstrap";
import { getIdFromToken } from "../../util/index";
import DeleteIcon from "../../assets/delete.png";
import { deleteBuild } from "../../redux/actions/buildActions";
import { connect } from "react-redux";

class BuildsListItem extends Component {
  handleDeleteBuild(id) {
    this.props.deleteBuild(id);
  }

  returnDeleteActionButton(id) {
    return (
      <Button
        onClick={() => this.handleDeleteBuild(id)}
        style={{ backgroundColor: "white", borderColor: "white" }}
      >
        <img alt="Delete" src={DeleteIcon} width="25px" height="25px" />
      </Button>
    );
  }

  render() {
    const { title } = this.props.buildItem;
    const buildId = this.props.buildItem._id;

    const { username, _id } = this.props.buildItem.user;

    return (
      <ListGroupItem style={{ flexDirection: "row", flex: 1 }}>
        <Row>
          <Col xs="8">
            <div className="col-8" style={{ color: "black" }}>
              <div>
                <p>
                  <b>{title}</b>
                </p>
              </div>
              <div>
                <small>
                  Created by: <b>{username}</b>
                </small>
              </div>
            </div>
          </Col>
          <Col xs="4">
            <div className="text-center">
              {getIdFromToken() === _id
                ? this.returnDeleteActionButton(buildId)
                : null}
            </div>
          </Col>
        </Row>
      </ListGroupItem>
    );
  }
}

const mapStateToProps = state => ({
  build: state.build,
  isAuthenticated: state.auth.isAuthenticated
});

export default connect(mapStateToProps, { deleteBuild })(BuildsListItem);
