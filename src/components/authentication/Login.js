import React, { Component, Fragment } from "react";
import LoginForm from "./LoginForm";
import { Container, Row, Col, Card, Jumbotron, CardBody } from "reactstrap";
import "../../App.css";

class Login extends Component {
  render() {
    return (
      <Fragment>
        <Container>
          <div>
            <Row>
              <Col />
              <Col lg="8">
                <Jumbotron>
                  <h3>Login to PC-Builder account</h3>
                  <hr />
                  <Card>
                    <CardBody>
                      <LoginForm />
                    </CardBody>
                  </Card>
                </Jumbotron>
              </Col>
              <Col />
            </Row>
          </div>
        </Container>
      </Fragment>
    );
  }
}

export default Login;
