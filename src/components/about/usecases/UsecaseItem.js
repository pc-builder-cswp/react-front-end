import React, { Component } from "react";
import PropTypes from "prop-types";

class UsecaseItem extends Component {
  render() {
    const uc = this.props.usecase;

    const _id = uc[0];
    const name = uc[1];
    const description = uc[2];
    const actor = uc[3];
    const precondition = uc[4];
    const scenarios = uc[5];
    const faulthyscenarios = uc[6];
    const postcondition = uc[7];

    return (
      <table className="table table-sm table-bordered">
        <tbody>
          <tr className="table-primary">
            <th scope="row" style={{ width: "16.66%" }}>
              Naam
            </th>
            <td>
              <strong>
                {_id} {name}
              </strong>
            </td>
          </tr>
          <tr>
            <th scope="row">Beschrijving</th>
            <td>{description}</td>
          </tr>
          <tr>
            <th scope="row">Actor</th>
            <td>{actor}</td>
          </tr>
          <tr>
            <th scope="row">Preconditie</th>
            <td>{precondition}</td>
          </tr>
          <tr>
            <th scope="row">Scenario</th>
            <td>
              <ol>
                {scenarios.map(step => (
                  <li key={step}>{step}</li>
                ))}
              </ol>
            </td>
          </tr>
          <tr>
            <th scope="row">Fout</th>
            <td>
              <ol>
                {faulthyscenarios.map(step => (
                  <li key={step}>{step}</li>
                ))}
              </ol>
            </td>
          </tr>
          <tr>
            <th scope="row">Postconditie</th>
            <td>{postcondition}</td>
          </tr>
        </tbody>
      </table>
    );
  }
}

// PropTypes
UsecaseItem.propTypes = {
  usecase: PropTypes.array.isRequired
};

export default UsecaseItem;
