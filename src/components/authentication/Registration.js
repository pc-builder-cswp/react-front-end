import React, { Component } from "react";
import RegistrationForm from "./RegistrationForm";
import { Container, Col, Row, Card, CardBody, Jumbotron } from "reactstrap";
import "../../App.css";

class Registration extends Component {
  render() {
    return (
      <Container>
        <Row>
          <Col />
          <Col lg="8">
            <Jumbotron>
              <h3>Create a new account on PC-Builder</h3>
              <hr />
              <Card>
                <CardBody>
                  <RegistrationForm />
                </CardBody>
              </Card>
            </Jumbotron>
          </Col>
          <Col />
        </Row>
      </Container>
    );
  }
}

export default Registration;
