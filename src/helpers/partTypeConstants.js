export const CPU = "cpu";
export const CPU_COOLER = "cpuCooler";
export const MOTHERBOARD = "motherboard";
export const RAM = "ram";
export const PSU = "psu";
export const STORAGE = "storage";
export const PC_CASE = "pcCase";
export const GPU = "gpu";
