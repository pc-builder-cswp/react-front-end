import React from "react";
import ReactDOM from "react-dom";
import Usecases from "../../../components/about/usecases/Usecases";
import { USECASES } from "../../../usecase-data";

it("Usecase renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(<Usecases usecases={USECASES} />, div);
  ReactDOM.unmountComponentAtNode(div);
});
