import React, { Component } from "react";
import NotFoundImage from "../../assets/not-found.jpg";
import { Container, ListGroupItem, Row } from "reactstrap";

class PartListItem extends Component {
  render() {
    const { name, price, partType, imgUrl } = this.props.partItem;

    // Check if imgUrl exists in prop.partItem
    const returnImage = () => {
      if (imgUrl == null) {
        return (
          <img
            style={styleSheet.img}
            src={NotFoundImage}
            alt="Part"
            width="63px"
            height="60px"
          />
        );
      } else {
        return (
          <Container>
            <img
              style={{
                justifyContent: "center",
                alignItems: "center",
                flex: 1
              }}
              src={imgUrl}
              alt={name}
              width="63px"
              height="60px"
            />
          </Container>
        );
      }
    };

    return (
      <ListGroupItem
        style={{
          flexDirection: "row",
          flex: 1,
          borderRadius: "5px",
          marginBottom: "20px"
        }}
      >
        <Row style={{ padding: "10px" }}>
          <div className="col-8" style={{ color: "black" }}>
            <p>
              <b>{name}</b>
            </p>
            <p>type: {partType}</p>
            <small>&euro;{price}</small>
          </div>
          <div className="col-4">{returnImage()}</div>
        </Row>
      </ListGroupItem>
    );
  }
}

const styleSheet = {
  img: {
    height: "60px",
    width: "63px",
    justifyContent: "center"
  }
};

export default PartListItem;
