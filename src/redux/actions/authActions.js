import Axios from "axios";
import { returnErrors } from "./errorActions";
import { history } from "../../helpers/history";

import {
  USER_LOADED,
  USER_LOADING,
  AUTH_ERROR,
  LOGIN_SUCCESS,
  LOGIN_FAIL,
  LOGOUT_SUCCESS,
  REGISTER_SUCCESS,
  REGISTER_FAIL,
  PASSWORD_UPDATED,
  UPDATING_PASSWORD
} from "./types";

// Check token & load user
export const loadUser = () => dispatch => {
  // User loading
  dispatch({ type: USER_LOADING });

  Axios({ method: "get", url: "/apiv1/users", headers: tokenConfig().headers })
    .then(res =>
      dispatch({
        type: USER_LOADED,
        payload: res.data.user
      })
    )
    .catch(err => {
      dispatch(returnErrors(err.response.data, err.response.status));
      dispatch({
        type: AUTH_ERROR
      });
    });
};

// Register User
export const register = ({ username, emailaddress, password }) => dispatch => {
  const config = {
    headers: {
      "Content-Type": "application/json"
    }
  };

  // Request body
  const body = JSON.stringify({ username, emailaddress, password });

  Axios.post("/auth/register", body, config)
    .then(res => {
      dispatch({
        type: REGISTER_SUCCESS,
        payload: res.data.user
      });
      localStorage.setItem("token", res.data.token);
      history.push("/account");
    })
    .catch(err => {
      dispatch(
        returnErrors(err.response.data, err.response.status, REGISTER_FAIL)
      );
      dispatch({
        type: REGISTER_FAIL
      });
    });
};

// Login User
export const login = ({ username, password }) => dispatch => {
  // Headers
  const config = {
    headers: {
      "Content-Type": "application/json"
    }
  };

  // Request body
  const body = JSON.stringify({ username, password });

  console.log("BODY: " + body);

  Axios.post("/auth/login", body, config)
    .then(res => {
      dispatch({
        type: LOGIN_SUCCESS,
        payload: res.data.user
      });
      localStorage.setItem("token", res.data.token);
      history.push("/");
    })
    .catch(err => {
      console.log(JSON.stringify(err));
      dispatch(
        returnErrors(err.response.data.message, err.response.status, LOGIN_FAIL)
      );
    });
};

// Update User's password
export const resetPassword = passObj => dispatch => {
  dispatch(setUpdatingPassword());
  Axios({
    method: "put",
    url: "apiv1/users/resetpassword",
    data: passObj,
    headers: tokenConfig().headers
  })
    .then(() => {
      dispatch({
        type: PASSWORD_UPDATED
      });
      history.push("/");
    })
    .catch(err => {
      console.log(JSON.stringify(err));
      dispatch(
        returnErrors(err.response.data, err.response.status, LOGIN_FAIL)
      );
    });
};

// Logout User
export const logout = () => {
  history.push("/login");
  return {
    type: LOGOUT_SUCCESS
  };
};

export const setUpdatingPassword = () => {
  return {
    type: UPDATING_PASSWORD
  };
};

// Setup config/headers and token
export const tokenConfig = () => {
  // Get token from localstorage
  let authToken = localStorage.getItem("token");

  // Headers
  const config = {
    headers: {
      "Content-Type": "application/json",
      token: authToken
    }
  };

  return config;
};
