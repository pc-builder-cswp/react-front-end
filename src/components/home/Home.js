import React, { Component } from "react";
import { connect } from "react-redux";
import { loadUser } from "../../redux/actions/authActions";
import { Container, Card, CardBody } from "reactstrap";
import { Link } from "react-router-dom";
import builderLogo from "../../assets/builder.png";
import builds from "../../assets/builds.png";
import gpuImage from "../../assets/gpu.png";
import userImage from "../../assets/user.png";
import PropTypes from "prop-types";
import {
  MORNING,
  AFTERNOON,
  EVENING,
  NIGHT,
  GREETING
} from "../../helpers/languageTextConstants";
import "../../App.css";

class Home extends Component {
  componentWillMount() {
    this.props.loadUser();
  }

  static propTypes = {
    auth: PropTypes.object.isRequired,
    isAuthenticated: PropTypes.bool
  };

  getGreetingsText() {
    try {
      let currentDateHours = new Date().getHours();
      if (currentDateHours >= 0 && currentDateHours <= 5) {
        return NIGHT;
      } else if (currentDateHours >= 6 && currentDateHours <= 11) {
        return MORNING;
      } else if (currentDateHours >= 12 && currentDateHours <= 17) {
        return AFTERNOON;
      } else {
        return EVENING;
      }
    } catch (error) {
      return GREETING;
    }
  }

  // NEED TO FIX REDUX USER PROP
  getNameOfUser() {
    const userName = this.props.auth.user.username;

    console.log("USERNAME" + userName);

    if (!userName) {
      return null;
    }

    return userName;
  }

  componentDidMount() {
    this.props.loadUser();
  }

  render() {
    return (
      <div>
        <nav>
          <div className="text-center">
            <h1>Home</h1>
            <p>{this.getGreetingsText()}</p>
          </div>
        </nav>
        <Container style={{ paddingTop: "20px", paddingBottom: "20px" }}>
          <div style={{ padding: "10px" }}>
            <Link to="/builder">
              <Card>
                <CardBody className="text-center">
                  <div>
                    <img
                      src={builderLogo}
                      alt="Builder"
                      width="80px"
                      height="80px"
                    />
                  </div>
                  <div>
                    <h5>Builder</h5>
                  </div>
                </CardBody>
              </Card>
            </Link>

            <Link to="/builds">
              <Card>
                <CardBody className="text-center">
                  <div>
                    <img src={builds} alt="Builds" width="80px" height="80px" />
                  </div>
                  <div>
                    <h5>Builds</h5>
                  </div>
                </CardBody>
              </Card>
            </Link>

            <Link to="/parts">
              <Card>
                <CardBody className="text-center">
                  <div>
                    <img
                      src={gpuImage}
                      alt="Parts"
                      width="80px"
                      height="80px"
                    />
                  </div>
                  <div>
                    <h5>Parts</h5>
                  </div>
                </CardBody>
              </Card>
            </Link>

            <Link to="/account">
              <Card>
                <CardBody className="text-center">
                  <div>
                    <img
                      src={userImage}
                      alt="User"
                      width="80px"
                      height="80px"
                    />
                  </div>
                  <div>
                    <h5>User</h5>
                  </div>
                </CardBody>
              </Card>
            </Link>
          </div>
        </Container>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.auth,
  isAuthenticated: state.auth.isAuthenticated,
  user: state.auth.user
});

export default connect(mapStateToProps, { loadUser })(Home);
