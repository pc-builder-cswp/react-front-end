import Axios from "axios";
import {
  GET_BUILD,
  BUILD_LOADING,
  CREATE_BUILD,
  DELETE_BUILD,
  GET_BUILDS,
  BUILDS_LOADING
} from "./types";
import { history } from "../../helpers/history";
import { tokenConfig } from "./authActions";
import { returnErrors } from "./errorActions";

export const getBuilds = () => dispatch => {
  dispatch(setBuildsLoading);

  Axios({ method: "get", url: "/apiv1/builds", headers: tokenConfig().headers })
    .then(res => {
      console.log(JSON.stringify(res.data));
      dispatch({
        type: GET_BUILDS,
        payload: res.data
      });
    })
    .catch(err => {
      console.log(JSON.stringify(err.response.data));
      dispatch(returnErrors(err.response.data, err.response.status));
    });
};

export const getBuild = id => dispatch => {
  dispatch(setBuildsLoading());
  Axios({
    method: "get",
    url: `/apiv1/builds/${id}`,
    headers: tokenConfig().headers
  })
    .then(res => {
      dispatch({
        type: GET_BUILD,
        payload: res.data
      });
    })
    .catch(err =>
      dispatch(returnErrors(err.response.data, err.response.status))
    );
};

export const createBuild = build => dispatch => {
  console.log(JSON.stringify(build));
  Axios({
    method: "post",
    url: "/apiv1/builds",
    data: build,
    headers: tokenConfig().headers
  })
    .then(res => {
      dispatch({
        type: CREATE_BUILD,
        payload: res.data.build
      });
      history.push(`/builds/${res.data.build._id}`);
    })
    .catch(err =>
      dispatch(returnErrors(err.response.data, err.response.status))
    );
};

export const deleteBuild = id => dispatch => {
  Axios({
    method: "delete",
    url: `/apiv1/builds/${id}`,
    headers: tokenConfig().headers
  })
    .then(res =>
      dispatch({
        type: DELETE_BUILD,
        payload: res.data.build
      })
    )
    .catch(err =>
      dispatch(returnErrors(err.response.data, err.response.status))
    );
};

export const setBuildsLoading = () => {
  return {
    type: BUILDS_LOADING
  };
};

export const setBuildLoading = () => {
  return {
    type: BUILD_LOADING
  };
};
