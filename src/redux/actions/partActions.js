import Axios from "axios";
import { GET_PARTS, PARTS_LOADING, PART_LOADING, GET_PART } from "./types";
import { tokenConfig } from "./authActions";
import { returnErrors } from "./errorActions";

export const getParts = () => dispatch => {
  dispatch(setPartsLoading());
  Axios({ method: "get", url: "/apiv1/parts", headers: tokenConfig().headers })
    .then(res => {
      dispatch({
        type: GET_PARTS,
        payload: res.data
      });
    })
    .catch(err => {
      console.log(JSON.stringify(err.response.data));
      dispatch(returnErrors(err.response.data, err.response.status));
    });
};

export const getPart = id => (dispatch, getState) => {
  dispatch(setPartLoading());
  Axios.get(`/apiv1/parts/${id}`, tokenConfig(getState))
    .then(res =>
      dispatch({
        type: GET_PART,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch(returnErrors(err.response.data, err.response.status))
    );
};

export const setPartsLoading = () => {
  return {
    type: PARTS_LOADING
  };
};

export const setPartLoading = () => {
  return {
    type: PART_LOADING
  };
};
