import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { logout } from "../../redux/actions/authActions";
import PropTypes from "prop-types";

class Logout extends Component {
  static propTypes = {
    logout: PropTypes.func.isRequired
  };

  render() {
    return (
      <Link to={"/login"} onClick={this.props.logout}>
        <li>Logout</li>
      </Link>
    );
  }
}

export default connect(null, { logout })(Logout);
