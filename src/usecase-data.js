const uc1 = [
	'UC-01',
	'Inloggen',
	'Hiermee logt een bestaande gebruiker in.',
	'Reguliere gebruiker',
	'Geen',
	[
		'1.	Gebruiker vult gebruikersnaam en wachtwoord in en klikt op Login knop.',
		'2.	De applicatie valideert de ingevoerde gegevens.',
		'3.	Als de inloggegevens kloppen, dan stuurt de applicatie de gebruiker naar het accountoverzicht van de gebruiker.'
	],
	[
		'1.	Gebruiker vult gebruikersnaam en wachtwoord in en klikt op Login knop.',
		'2.	De applicatie valideert de ingevoerde gegevens.',
		'3.	De applicatie geeft aan dat de inloggegevens niet kloppen.',
		'Resultaat: De gebruiker is niet ingelogd.'
	],
	'De gebruiker is ingelogd.'
];

const uc2 = [
	'UC-02',
	'Registreren',
	'Hiermee registreert een nieuwe gebruiker een account.',
	'Reguliere gebruiker',
	'Geen',
	[
		'1.	Gebruiker klikt op de Registreren knop op de homepagina of op de loginscherm, vult de benodigde gegevens in en klikt op ‘Register’.',
		'2.	De applicatie valideert de ingevoerde gegevens.',
		'3.	Als de registratiegegevens kloppen, dan redirect de applicatie de gebruiker naar de accountoverzicht.'
	],
	[
		'1.	1.	Gebruiker klikt op de Registreren knop op de homepagina of op de loginscherm, vult de benodigde gegevens in en klikt op ‘Register’.',
		'2.	De applicatie valideert de ingevoerde gegevens.',
		'3.	De applicatie toont de gebruiker al bestaat of gegevens zijn incorrect.',
		'Resultaat: De gebruiker is niet geregistreerd.'
	],
	'De gebruiker is ingelogd.'
];

const uc3 = [
	'UC-03',
	'Onderdelen bekijken',
	'Hiermee ziet de gebruiker een lijst van onderdelen.',
	'Reguliere gebruiker',
	'Geen',
	[
		'1.	Gebruiker gaat naar via de navigatie naar ‘Parts’.',
		'2.	De gebruiker kan kiezen op soort onderdeel.',
		'3.	Als de gebruiker op een soort onderdeel klikt, dan ziet hij een lijst met de onderdelen.'
	],
	[],
	'De gebruiker is ingelogd.'
];

const uc4 = [
	'UC-04',
	'Details van een onderdeel bekijken',
	'Hiermee kan een gebruiker de details van een onderdeel bekijken.',
	'Reguliere gebruiker',
	'Geen',
	[
		'1.	UC-03 ',
		'2.	De gebruiker klikt op een onderdeel',
		'3.	De applicatie haalt de gegevens op en toont de gebruiker de gegevens.'
	],
	[],
	'De gebruiker ziet de details van een onderdeel.'
];

const uc5 = [
	'UC-05',
	'Build aanmaken',
	'Hiermee kan een gebruiker een PC Build initialiseren.',
	'Ingelogde gebruiker',
	'Geen',
	[
		'1.	Gebruiker gaat via de navigatie naar ‘Build a PC’.',
		'2.	De applicatie toont de Build pagina en de gebruiker klikt op “Build now”.',
		'3.	De applicatie laadt een form waarin de gebruiker de benodigde gegevens invult en klikt op ‘Confirm’.',
		'4.	De applicatie maakt een nieuwe PC Build aan.'
	],
	[],
	'De gebruiker heeft een nieuwe PC Build geïnitialiseerd.'
];

const uc6 = [
	'UC-06',
	'Part toevoegen aan een Build',
	'Hiermee voegt een gebruiker een onderdeel toe aan zijn PC Build.',
	'Ingelogde gebruiker',
	'De gebruiker heeft een PC Build aangemaakt waarin hij het onderdeel toevoegt.',
	[
		'1.	UC-03',
		'2.	UC-04',
		'3.	De gebruiker klikt op ‘Add’ en het onderdeel wordt toegevoegd aan de build of wordt vervangen met het vorige onderdeel.',
		'4.	De applicatie stuurt de gebruiker terug naar zijn PC Build.'
	],
	[],
	'De gebruiker heeft een onderdeel aan zijn PC Build toegevoegd.'
];

const uc7 = [
	'UC-07',
	'Part verwijderen van een Build',
	'Hiermee verwijdert een gebruiker een onderdeel van zijn PC Build.',
	'Ingelogde gebruiker',
	'De applicatie toont de scherm van de PC Build van de gebruiker.',
	[
		'1.	De gebruiker klikt op het kruisje naast het onderdeel dat diegene wilt verwijderen.',
		'2.	De applicatie verwijdert het geselecteerde onderdeel van de Build.'
	],
	[],
	'De gebruiker heeft een onderdeel aan zijn PC Build verwijderd.'
];

const uc8 = [
	'UC-08',
	'Lijst van andere Builds weergeven',
	'Hiermee kan een gebruiker andere Builds zien van andere gebruikers.',
	'Reguliere gebruiker',
	'Geen',
	[
		'1.	De gebruiker gaat via de navigatie naar ‘Completed Builds’.',
		'2.	De applicatie toont een lijst met Builds van andere gebruikers.'
	],
	[],
	'De gebruiker ziet een lijst van Builds van andere gebruikers.'
];

const uc9 = [
	'UC-09',
	'Details van een andere Build weergeven.',
	'Hiermee kan een gebruiker de details van een andere Builds zien.',
	'Reguliere gebruiker',
	'Geen',
	[
		'1.	UC-08',
		'2.	De gebruiker klikt op een Build.',
		'3.	De applicatie toont de details van de geselecteerde Build.'
	],
	[],
	'De gebruiker ziet een de details van een Build.'
];

const uc10 = [
	'UC-10',
	'Een build opvoten',
	'Hiermee kan een gebruiker de details van een andere Builds zien.',
	'Reguliere gebruiker',
	'Geen',
	[
		'1.	UC-09',
		'2.	De gebruiker klikt op ‘Upvote’.',
		'3.	De applicatie toont dat de gebruiker de Build heeft upgevoted.'
	],
	[],
	'De gebruiker heeft een upvote gegeven aan een Build.'
];

export const USECASES = [ uc1, uc2, uc3, uc4, uc5, uc6, uc7, uc8, uc9, uc10 ];
