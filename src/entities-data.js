const user = [
  "User",
  "userName: string",
  "password: string",
  "emailAddress: string",
  "registerDate: Date"
];

const build = [
  "Build",
  "user: objectId",
  "postDate: Date",
  "title: string",
  "description: string",
  "cpu: objectId",
  "cpuCooler: objectId",
  "motherboard: objectId",
  "memory: objectId",
  "storage: objectId",
  "gpu: objectId",
  "pcCase: objectId",
  "psu: objectId",
  "operatingSystem: objectId"
];

const part = [
  "Part",
  "name: String",
  "description: String",
  "price: Number",
  "manufacturer: String",
  "partType: objectId",
  "powerUsage: Number",
  "releaseDate: Date"
];

const comment = [
  "Comment",
  "user: objectId",
  "build: objectId",
  "part: objectId",
  "content: String"
];

export const ENTITIES = [user, build, part, comment];
