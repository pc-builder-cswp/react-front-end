import React from "react";
import ReactDOM from "react-dom";
import UsecaseItem from "../../../components/about/usecases/UsecaseItem";
import { USECASES } from "../../../usecase-data";

it("UsecaseItem renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(<UsecaseItem usecase={USECASES[0]} />, div);
  ReactDOM.unmountComponentAtNode(div);
});
