import React, { Component } from "react";
import { Container, Card, CardBody, Row, Jumbotron } from "reactstrap";
import { getBuild } from "../../redux/actions/buildActions";
import PartListItem from "../parts/PartListItem";
import CommentList from "../comment/CommentList";
import { BUILD } from "../../helpers/objectTypesConstants";
import { connect } from "react-redux";

class BuildDetail extends Component {
  getParamsId() {
    const {
      match: { params }
    } = this.props;

    return params.id;
  }

  componentDidMount() {
    this.props.getBuild(this.getParamsId());
  }

  returnImage(imgUrl) {
    if (imgUrl) {
      return (
        <img
          src={imgUrl}
          style={{
            borderRadius: "5px",
            borderWidth: "1px",
            borderColor: "lightgrey"
          }}
          alt="part"
          width="75px"
          height="75px"
        />
      );
    } else {
      return <img src="../../assets/not-found.jpg" alt="part" />;
    }
  }

  render() {
    if (!this.props.build.build) {
      return (
        <Container>
          <Card>
            <CardBody>
              <div className="text-center">
                <h5>Could not get Build details</h5>
              </div>
            </CardBody>
          </Card>
        </Container>
      );
    } else {
      const {
        title,
        description,
        cpu,
        cpuCooler,
        motherboard,
        memory,
        psu,
        pcCase,
        storage,
        gpu
      } = this.props.build.build;

      const userName = this.props.build.build.user.username;

      return (
        <div>
          <nav className="text-center">
            <div>
              <h1>{title}</h1>
              <p>Created by: {userName}</p>
            </div>
          </nav>

          <Container>
            <div>
              <Jumbotron>
                <Row>
                  <Container>
                    <h3>
                      <b>Description</b>
                    </h3>
                    <Row />
                    <p>{description}</p>
                  </Container>
                </Row>
              </Jumbotron>

              <div style={{ marginTop: "20px", marginBottom: "10px" }}>
                <h4>
                  <b>Parts that are used in this build</b>
                </h4>
              </div>

              <PartListItem partItem={cpu} />

              <PartListItem partItem={cpuCooler} />

              <PartListItem partItem={motherboard} />

              <PartListItem partItem={memory} />

              <PartListItem partItem={storage} />

              <PartListItem partItem={gpu} />

              <PartListItem partItem={pcCase} />

              <PartListItem partItem={psu} />

              <div>
                <CommentList
                  componentId={this.getParamsId()}
                  componentType={BUILD}
                />
              </div>
            </div>
          </Container>
        </div>
      );
    }
  }
}

const mapStateToProps = state => ({
  build: state.build,
  isAuthenticated: state.auth.isAuthenticated
});

export default connect(mapStateToProps, { getBuild })(BuildDetail);
